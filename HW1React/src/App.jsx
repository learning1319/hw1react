import { useState } from "react";
import "./Modal/Modal.scss";
import "./App.css";
import Button from "./Button/Button";
import ModalImage from "./Modal/ModalImage";
import ModalText from "./Modal/ModalText";
import modalImg from "./assets/modalImg.png";

function App() {
  const [showFirstModal, setShowFirstModal] = useState(false);
  const [showSecondModal, setShowSecondModal] = useState(false);
  

  const openFirstModal = () => {
    setShowFirstModal(true);
  };

  const openSecondModal = () => {
    setShowSecondModal(true);
  };
  const closeModal = (e) => {
    setShowFirstModal(false);
    setShowSecondModal(false);
   
  };
 
 
  return (
    <>
     {!showFirstModal && !showSecondModal && (
        <>
          <Button classNames="button-start" onClick={openFirstModal}>
            Open first modal
          </Button>
          <Button classNames="button-start" onClick={openSecondModal}>
            Open second modal
          </Button>
        </>
      )}
      {showFirstModal && <ModalImage imageUrl={modalImg} alt="Image" title="Product Delete!" text="By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted." onClose={closeModal} />}
      {showSecondModal && <ModalText title="Add Product “NAME”" text="Description for you product" onClose={closeModal}  />}
      
    </>
  );
}

export default App;
