export default function ModalWraper({children, onClick}){
    const handleClick = (e) => {
        if (e.target === e.currentTarget) {
          onClick();
        }
      };
    return <div className="modal-wraper" onClick={handleClick}>{children}</div>
}