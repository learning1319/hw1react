import ModalClose from "./ModalClose";
import ModalFooter from "./ModalFooter";
import ModalHeader from "./ModalHeader";
import ModalWraper from "./ModalWraper";
import ModalBody from "./ModalBody";


export default function Modal({children, onClose}){
return(
    <>
  <ModalWraper onClick={onClose}>
  <div className="modal">
        <ModalHeader>
          <ModalClose onClick={onClose}/>
        </ModalHeader>
        <ModalBody>{children}</ModalBody>
        
      </div>
  </ModalWraper>
  
    </>
)
}