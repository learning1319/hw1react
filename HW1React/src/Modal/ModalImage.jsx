import Modal from "./Modal"
import ModalFooter from "./ModalFooter";

export default function ModalImage({ imageUrl, alt, title, text, onClose }) {
    return (
      <Modal onClose={onClose}>
        <img src={imageUrl} alt={alt} />
        <h2>{title}</h2>
        <p>{text}</p>
        <ModalFooter
        firstText = {"No, cancel".toUpperCase()}
        secondaryText = {"Yes, delete".toUpperCase()}
        firstClick={() => setShowFirstModal(false)}
        // secondaryClick={() => { /* Логіка підтвердження */ }}
      />
      </Modal>
    );
  }