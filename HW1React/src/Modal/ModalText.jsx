import Modal from "./Modal"
import ModalFooter from "./ModalFooter";

export default function ModalText({ title, text, onClose }) {
    return (
      <Modal onClose={onClose} >
        <h2>{title}</h2>
        <p>{text}</p>
        <ModalFooter
        firstText = {"add to favorite".toUpperCase()}
        
        firstClick={() => setShowFirstModal(false)}
        // secondaryClick={() => { /* Логіка підтвердження */ }}
      />
      </Modal>
    );
  }