import React from 'react';

export default function ModalClose({ onClick }) {
  return (
    <button className="modal_close-button" onClick={onClick}>
      &#10006; 
    </button>
  );
}


